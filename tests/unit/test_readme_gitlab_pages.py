from gcip import PagesJob, Pipeline
from tests import conftest
from gcip.addons.gitlab.jobs.pages import AsciiDoctor


def test():
    pipeline = Pipeline()
    pipeline.add_children(
        AsciiDoctor(source="docs/index.adoc", out_file="/index.html"),
        PagesJob(),
    )

    conftest.check(pipeline.render())
