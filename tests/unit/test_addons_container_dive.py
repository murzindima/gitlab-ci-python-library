from gcip import Pipeline
from tests import conftest
from gcip.addons.container.jobs import dive


def test_default_dive_job():
    pipeline = Pipeline()

    job = dive.Scan()
    job.image_path

    pipeline.add_children(
        dive.Scan(),
        name="default",
    )
    pipeline.add_children(
        dive.Scan(image_path="/absolute/path/", image_name="image_name"),
        name="custom_image_and_path",
    )
    pipeline.add_children(
        dive.Scan(
            highest_user_wasted_percent=0.1,
            highest_wasted_bytes=0.2,
            lowest_efficiency=0.3,
            ignore_errors=True,
        ),
        name="custom_settings",
    )

    conftest.check(pipeline.render())
