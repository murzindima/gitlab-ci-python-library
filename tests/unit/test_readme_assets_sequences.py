import gcip
from tests import conftest
from gcip.addons.aws.sequences.cdk import DiffDeploy


def test():
    pipeline = gcip.Pipeline()
    sequence = DiffDeploy(stacks=["my-cdk-stack"])
    sequence.deploy_job.toolkit_stack_name = "cdk-toolkit"
    pipeline.add_children(sequence)

    conftest.check(pipeline.render())
